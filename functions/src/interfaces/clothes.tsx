export interface ClothingItemInterface {
  color: string;
  name: string;
  type: string;
  id: string;
}
