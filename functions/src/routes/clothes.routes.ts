module.exports = (app) => {
  const clothes = require('../controllers/clothes.controller.js');

  // Create a new Note
  app.post('/api/clothes', clothes.create);

  // Retrieve all clothes
  app.get('/api/clothes', clothes.findAll);

  // Retrieve a single Note with clothesId
  app.get('/api/clothes/:clothesId', clothes.findOne);

  // Update a Note with noteId
  app.put('/api/clothes/:clothesId', clothes.update);

  // Delete a Note with noteId
  app.delete('/api/clothes/:clothesId', clothes.remove);
}