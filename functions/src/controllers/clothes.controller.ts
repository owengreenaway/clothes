import { admin } from "../database";
import { Request, Response } from "express";

exports.findAll = (req: Request, res: Response) => {
  admin.database().ref("/clothes").once("value", function(snapshot) {
    const clothingItems = Object.keys(snapshot.val()).map( (clothingId: string) => {
      const clothingItem = snapshot.val()[clothingId];
      return {
        id: clothingId,
        name: clothingItem.name,
        type: clothingItem.type,
        color: clothingItem.color,
      }
    })
    
    res.json(clothingItems);
  }, function (errorObject) {
    res.json({"message": "The read failed: " + errorObject.code});
  });    
};

exports.findOne = (req: Request, res: Response) => {
  admin.database().ref(`/clothes/${req.params.clothesId}`).once("value", function(snapshot) {
    res.json(snapshot.val());
  }, function (errorObject) {
    res.json({"message": "The read failed: " + errorObject.code});
  });    
};

exports.update = (req: Request, res: Response) => {
  admin.database().ref(`/clothes/${req.params.clothesId}`).once("value", function(snapshot) {
    // TODO: crashes if snapshot.val() is null
    const clothingItem = snapshot.val();
    Object.assign(clothingItem, req.body)

    admin.database().ref(`/clothes/${req.params.clothesId}`).set(clothingItem).then(() => {
      res.json(clothingItem);
    }, function (errorObject) {
      res.json({"message": "The set failed: " + errorObject.code});
    });

  }, function (errorObject) {
    res.json({"message": "The read failed: " + errorObject.code});
  });    
};

exports.create = (req: Request, res: Response) => {
  // if(!req.body.name) {
  //   res.status(400).send({
  //       message: "Clothing name can not be empty"
  //   });
  //   return
  // }
  const clothingItem = {
    name: req.body.name || null,
    type: req.body.type || null,
    color: req.body.color || null,
  };
  admin.database().ref("/clothes").push(clothingItem).then(() => {
    res.json(clothingItem);
  });
};

exports.remove = (req: Request, res: Response) => {
  admin.database().ref(`/clothes/${req.params.clothesId}`).once("value", function(snapshot) {
    if (snapshot.val() === null) {
      res.status(404).send({
        message: "Clothing item not found with id " + req.params.clothesId
      })
      return
    }

    admin.database().ref(`/clothes/${req.params.clothesId}`).remove().then(() => {
      res.json(snapshot.val());
    });
  }, function (errorObject) {
    res.json({"message": "The read failed: " + errorObject.code});
  });    
};