import { admin } from "./database";

const express = require('express');
const bodyParser = require('body-parser');
const functions = require('firebase-functions');
const cors = require('cors')({origin: true});

admin.initializeApp();

const app = express();
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())
app.use(cors)
require('./routes/clothes.routes.js')(app);
require('./routes/outfits.routes.js')(app);

exports.api = functions.https.onRequest(app)