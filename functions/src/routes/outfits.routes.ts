module.exports = (app) => {
  const outfits = require('../controllers/outfits.controller.js');

  // Create a new Note
  app.post('/api/outfits', outfits.create);

  // Retrieve all outfits
  app.get('/api/outfits', outfits.findAll);

  // Retrieve a single Note with outfitsId
  app.get('/api/outfits/:outfitId', outfits.findOne);

  // Update a Note with noteId
  app.put('/api/outfits/:outfitId', outfits.update);

  // Delete a Note with noteId
  app.delete('/api/outfits/:outfitId', outfits.remove);

  // Retrieve worn history for an outfit
  app.delete('/api/outfits/:outfitId/history', outfits.findAllHistory);
};