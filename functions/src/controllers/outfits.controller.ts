import { admin } from "../database";
import { Request, Response } from "express";

exports.findAll = (req: Request, res: Response) => {
  admin.database().ref("/").once("value", function(snapshot) {
    if (snapshot.val() === null) {
      return res.json({"error": "The database is empty"});
    }

    const clothes = snapshot.val().clothes;
    const outfits = snapshot.val().outfits;

    if (clothes === undefined) {
      return res.json([]);  
    }

    if (outfits === undefined) {
      return res.json([]);  
    }

    const getClothingItem = (clothingId: string) => {
      const clothingItem = clothes[clothingId];
      if (clothingItem === undefined) {
        return `${clothingId} not found`
      }
      return {
        id: clothingId,
        name: clothingItem.name,
        type: clothingItem.type,
        color: clothingItem.color,
      }
    };

    const outfitsArray = Object.keys(outfits).map( (outfitId: string) => {
      const outfit = outfits[outfitId];
      return {
        id: outfitId,
        clothingItems: outfit.clothingItems.map(getClothingItem)
      }
    });

    return res.json(outfitsArray);     

    // res.json(populateOutfitsArray);
  }, function (errorObject) {
    return res.json({"message": "The read failed: " + errorObject.code});
  });
};

exports.findOne = (req: Request, res: Response) => {
  admin.database().ref(`/outfits/${req.params.outfitId}`).once("value", function(snapshot) {
    // res.json(snapshot.val());
    if (snapshot.val() === null) {
      res.status(404).send({
        message: "Outfit item not found with id " + req.params.outfitId
      });
      return
    }

    const outfit = snapshot.val();

    admin.database().ref('/clothes/').once("value", function(clothesSnapshot) {
      // res.json(clothesSnapshot.val());

      const clothes = clothesSnapshot.val();
      const outfitClothingIds = JSON.parse(outfit.clothingItems);

      res.send({
        clothes: clothes,
        clothingItems: outfitClothingIds,
        firstId: outfitClothingIds[0],
        first: clothes[outfitClothingIds[0]]
      })

    }, function (errorObject) {
      res.json({"message": "The read failed clothes: " + errorObject.code});
    });

  }, function (errorObject) {
    res.json({"message": "The read failed outfit: " + errorObject.code});
  });

};

exports.update = (req: Request, res: Response) => {
  admin.database().ref(`/outfits/${req.params.outfitId}`).once("value", function(snapshot) {
    const clothingItem = snapshot.val();
    if (clothingItem === null) {
      res.status(404).send({
        message: "Outfit item not found with id " + req.params.outfitId
      });
      return
    }
    Object.assign(clothingItem, req.body);

    admin.database().ref(`/outfits/${req.params.outfitId}`).set(clothingItem).then(() => {
      res.json(clothingItem);
    }, function (errorObject) {
      res.json({"message": "The set failed: " + errorObject.code});
    });

  }, function (errorObject) {
    res.json({"message": "The read failed: " + errorObject.code});
  });
};

exports.create = (req: Request, res: Response) => {
  if (req.body.clothingItems === undefined) {
    return res.status(400).send({
      message: "clothingItems must be defined"
    });
  }
  
  if (req.body.clothingItems.length !== 4) {
    return res.status(400).send({
      message: `clothingItems must have length 4. Received length ${req.body.clothingItems.length}`
    });
  }

  const outfit = {
    clothingItems: req.body.clothingItems,
    worn: [],
    washed: []
  };

  return admin.database().ref("/outfits").push(outfit).then(() => {
     res.status(201).json(outfit);
  });
};

exports.remove = (req: Request, res: Response) => {
  admin.database().ref(`/outfits/${req.params.outfitId}`).once("value", function(snapshot) {
    if (snapshot.val() === null) {
      res.status(404).send({
        message: "Outfit item not found with id " + req.params.outfitId
      });
      return
    }

    admin.database().ref(`/outfits/${req.params.outfitId}`).remove().then(() => {
      return res.json(snapshot.val());
    });
  }, function (errorObject) {
    return res.json({"message": "The read failed: " + errorObject.code});
  });
};

exports.findAllHistory = (req: Request, res: Response) => {
  admin.database().ref(`/outfits/${req.params.outfitId}/worn`).once("value", function(snapshot) {
    res.json(snapshot.val());
  }, function (errorObject) {
    res.json({"message": "The read failed: " + errorObject.code});
  });
};