# Backend for clothing and outfit tracking

## Introduction

Backend api for outfit tracking. Created using Node, Express, Firebase cloud functions, and a Firebase realtime database. Also includes a simple deployment pipeline for both the cloud functions and static assets.

## Next steps

I may add the frontend to this repo later or it might be it's own repo. Depends on my experiments with xState and Redux-thunks, and how experimental I want the frontend to be.